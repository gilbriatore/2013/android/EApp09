package br.up.edu.eapp09;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular(View v){

        EditText cxTempo = (EditText) findViewById(R.id.txtTempo);
        EditText cxVeloc = (EditText) findViewById(R.id.txtVelocidade);
        EditText cxConsumo = (EditText) findViewById(R.id.txtConsumo);

        double tempo = Double.parseDouble(cxTempo.getText().toString());
        double veloc = Double.parseDouble(cxVeloc.getText().toString());

        double consumo = (veloc * tempo) / 12;

        cxConsumo.setText(String.format("%.2f", consumo));

    }
}
